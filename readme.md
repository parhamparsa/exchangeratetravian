## Table of contents

* [Problem](#problem)

* [Available Solutions](#available-solutions)

* [Solution](#solution)

* [Implementation features](#implementation-features)

* [Installation](#installation)

* [Next features](#next-features)

### Problem ###
Design an exchange rate service, that has a base exchange currency and is able to calcuate other currencies based on base curreny..
Considerations:
* Database needs to collect the rate changes.
* A command to run hourly for getting the data from our provider.
* Create an API to get the data base on currency and time.
* Dockerizing the application

### Available Solutions ###
We have two solutions here
##### Getting Data Each Time, The Command Run And Save It On Database As A New Record

	Pros:
	We have history of all data

	Cons:
	The usage of disk is high, and we have a lot of records
	indexs on database needs more space and get bigger and bigger
##### •	Update the records if it's already exist and create new row if the currency does not exist
	Pros:
	Smaller table with less space needs and faster for search

	Cons:
	Previous data are not trackable.

### Solution ###
###### Database design :
We need two table here:


- `Currency Table: `
	This table hold all currency we have on our system with the flag that show the currency is base or not. In this table we have a column that must be index. and thats `is_base`. We could have saved this base currenty in a table like settings so that we don't need index a bool column. indexing a bool column create a big BTree in mysql but because this available currencies are quite small we implemented in this way.  

- `Currency Rate Table: `
	This table hold the relation between base currency and symbols and also the rate and datetime. This table gets updated with fetching updated records from our provide. We have also need to have a multi-column index on follwing columns (base , symbols, datetime)


###### Command:
We should run a cron job in a Docker container using supervisord to run a command to synchronize currency data every hour. We address the crontab file out of the container after installing supervisord to set a schedule for executing the list of and commands on a regular basis.

	class SyncCurrencyRateCommand extends Command

For the next step this can also be done automatically. Using either a bash script or more simply yaml ref

###### Repository Pattern:
We are using repository pattern for fetching data from the database.

###### API:
Create and API with assertion to fetch data from database and return the josn for result.’

![img.png](img.png)

### Test ###
We have implemneted a few unit tests for this project. In the next step adding integration tests and increasing code coverage is a must in this project.

### Implementation features ###
###### Used Tools:

Programming language: PHP8

Containerization: Docker, docker-compose

Framework: Symfony

### Project Structure:

* ├──DockerFiles
* ├──migrations
* ├──src
  * ├── Command
  * └── Contract
      * ├── Convertor
      * ├── Helper
      * ├── Interpreters
  * ├── Controller
  * ├── Domain
  * ├── Entity
  * ├── Repository
  * ├── View
* ├──test
* ├──templates

### Installation ###
Make command:

	up : run docker-compose up on all services
	down : run docker-compose down on all services
	install : install composer

### Next Features
Because this was project was quite small we didn't implement a lot of features. But here is some features that can be added if we are supposed to work this project and consider it a big project.
- Using DDD for implementation.
- Addingg BFF(backedn-for-frontend) so that
- Adding UI so that we can have different charts. the last update time, configurations and etc...
- Implement AAA(Accounting/Authentication/Authorization). There are many options such as JWT, OAuth and ...
- Using kubernetes job scheduler instead of crontab or docker. We can configure different jobs using helm when we use kubernetes
- Increase code coverage. Currently we have added just a few tests with an small code coverage. This can be increased to at lest 90%.




