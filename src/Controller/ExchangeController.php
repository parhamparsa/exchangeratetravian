<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Entity\CurrencyRate;
use App\Repository\CurrencyRateRepository;
use App\Repository\CurrencyRepository;
use Assert\Assert;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends AbstractController
{
    #[Route('/api/v1/exchange', name: 'exchange', methods: ["GET"])]
    public function getExchangeAction(Request $request): JsonResponse
    {
        $requestObject = $request->query->all();

        Assert::lazy()
            ->that($requestObject['currency'])
            ->notEmpty()
            ->string()
            ->that($requestObject['date_time'])
            ->notEmpty()
            ->date('Y-m-d H:i:s')
            ->verifyNow();

        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $this->getDoctrine()->getRepository(Currency::class);
        $currencyEntity = $currencyRepository->findOneBy(['name' => $requestObject['currency']]);

        if (!$currencyEntity) {
            throw new \Exception('requested currency is not exist');
        }

        /** @var CurrencyRateRepository $currencyRateRepository */
        $currencyRateRepository = $this->getDoctrine()->getRepository(CurrencyRate::class);
        $currencyRateEntity = $currencyRateRepository->findByCurrencyAndDatetime(
            $currencyEntity, $requestObject['date_time']
        );

        return new JsonResponse([
            'rate' => $currencyRateEntity->getRate(),
            'name' => $currencyEntity->getName(),
            'date_time' => Carbon::parse($currencyRateEntity->getSyncAt())->format('Y-m-d H:m:s')
        ]);
    }
}
