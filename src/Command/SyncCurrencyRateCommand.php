<?php

namespace App\Command;

use App\Contract\Convertor\CurrencyNameConverter;
use App\Contract\Helper\ExchangeRequest;
use App\Entity\Currency;
use App\Entity\CurrencyRate;
use App\Repository\CurrencyRateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'sync:currency:rate:command',
    description: 'Add a short description for your command',
)]
class SyncCurrencyRateCommand extends Command
{
    protected static $defaultName = 'sync:currency:rate:command';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ExchangeRequest        $exchangeRequest,
        private CurrencyNameConverter  $currencyNameConverter)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('sync currency per hour');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $currencyRepository = $this->entityManager->getRepository(Currency::class);

            /** @var Currency $baseCurrency */
            $baseCurrency = $currencyRepository->findOneBy(['is_base' => true]);

            if ($baseCurrency === null) {
                throw new Exception('No base currency found in the database!');
            }

            $nonBasedCurrencyEntities = $currencyRepository->findBy(['is_base' => false]);
            $nonBasedCurrencies = $this->currencyNameConverter->convert($nonBasedCurrencyEntities);

            $exchangeResponse = $this->exchangeRequest
                ->setBaseCurrency($baseCurrency->getName())
                ->setSymbols($nonBasedCurrencies)
                ->fetchRates();

            /** @var CurrencyRateRepository $currencyRateRepo */
            $currencyRateRepo = $this->entityManager->getRepository(CurrencyRate::class);

            /** @var Currency $nonBasedCurrency */
            foreach ($nonBasedCurrencyEntities as $nonBasedCurrency) {
                $destCurrency = $exchangeResponse->get($nonBasedCurrency->getName());

                //If we need the history of all rate we can skip this part of code and only use (new CurrencyRate) as I mentioned in document
                $currencyRate = $currencyRateRepo->findByBaseAndCurrentCurrency($baseCurrency,$nonBasedCurrency) ?? new CurrencyRate();

                $currencyRate
                    ->setBaseCurrencyId($baseCurrency)
                    ->setDestinationCurrencyId($nonBasedCurrency)
                    ->setRate($destCurrency->getRate())
                    ->setSyncAt($destCurrency->getDateTime());
                $this->entityManager->persist($currencyRate);
            }

            $this->entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        return self::SUCCESS;
    }
}
