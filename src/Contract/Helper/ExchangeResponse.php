<?php

namespace App\Contract\Helper;

use App\Domain\Currency;

class ExchangeResponse
{
    public function __construct(private array $data = [])
    {
    }

    public function add(Currency $currency)
    {
        $this->data[$currency->getName()] = $currency;
    }

    public function clear()
    {
        $this->data = [];
    }

    public function get(string $name): Currency
    {
        return $this->data[$name];
    }
}