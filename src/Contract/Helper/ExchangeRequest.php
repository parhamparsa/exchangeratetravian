<?php

namespace App\Contract\Helper;

use App\Contract\Interpreters\ExchangeResponseInterpreter;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ExchangeRequest
{
    const FIXER_URL = 'http://data.fixer.io/api/latest';

    private string $baseCurrency;
    private array $symbols;

    public function __construct(private ExchangeResponseInterpreter $interpreter)
    {
    }

    public function setBaseCurrency(string $baseCurrency): self
    {
        $this->baseCurrency = $baseCurrency;
        return $this;
    }

    public function setSymbols(array $symbols): self
    {
        $this->symbols = $symbols;
        return $this;
    }

    /**
     * @return ExchangeResponse
     * @throws Exception
     */
    public function fetchRates(): ExchangeResponse
    {
        try {
            $client = new Client();

            $httpResponse = $client->get(self::FIXER_URL, $this->getPayload());

            return $this->interpreter->interpret($httpResponse);

        } catch (GuzzleException $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    private function getPayload(): array
    {
        return [
            'query' => [
                'access_key' => $_ENV['ACCESS_KEY'],
                'base' => $this->baseCurrency,
                'symbols' => implode(',', $this->symbols)
            ]
        ];
    }
}
