<?php

namespace App\Contract\Convertor;

use App\Entity\Currency;

class CurrencyNameConverter
{
    /**
     * @param Currency[] $currencies
     * @return string[]
     */
    public function convert(array $currencies): array
    {
        $names = [];

        foreach ($currencies as $currency) {
            $names[] = $currency->getName();
        }

        return $names;
    }
}