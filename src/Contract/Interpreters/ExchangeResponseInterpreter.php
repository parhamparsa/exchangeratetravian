<?php

namespace App\Contract\Interpreters;

use App\Contract\Helper\ExchangeResponse;
use App\Domain\Currency;
use Carbon\Carbon;
use Psr\Http\Message\ResponseInterface;

class ExchangeResponseInterpreter
{
    public function interpret(ResponseInterface $response): ExchangeResponse
    {
        $exchangeResponse = new ExchangeResponse();
        $result = json_decode($response->getBody()->getContents(), true);

        if (isset($result['success']) && $result['success'] === true) {
            foreach ($result['rates'] as $currencyName => $rate) {
                $exchangeResponse->add(new Currency($currencyName, $rate, Carbon::now()));
            }
        }
        return $exchangeResponse;
    }
}