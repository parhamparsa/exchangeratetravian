<?php

namespace App\Domain;

class Currency
{
    public function __construct(private string $name, private float $rate, private \DateTime $dateTime)
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }
}