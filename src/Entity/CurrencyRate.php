<?php

namespace App\Entity;

use App\Repository\CurrencyRateRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;


/**
 * @ORM\Entity(repositoryClass=CurrencyRateRepository::class)
 * @ORM\Table(name="currency_rates", indexes={@Index(name="currency_rate_idx", columns={"base_currency_id","destination_currency_id", "sync_at"})})
 */
class CurrencyRate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class)
     * @ORM\JoinColumn(nullable=false,name="base_currency_id", referencedColumnName="id")
     */
    private $base_currency_id;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class)
     * @ORM\JoinColumn(nullable=false,name="destination_currency_id", referencedColumnName="id")
     */
    private $destination_currency_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $sync_at;

    /**
     * @ORM\Column(type="float")
     */
    private $rate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBaseCurrencyId(): ?Currency
    {
        return $this->base_currency_id;
    }

    public function setBaseCurrencyId(?Currency $base_currency_id): self
    {
        $this->base_currency_id = $base_currency_id;

        return $this;
    }

    public function getDestinationCurrencyId(): ?Currency
    {
        return $this->destination_currency_id;
    }

    public function setDestinationCurrencyId(?Currency $destination_currency_id): self
    {
        $this->destination_currency_id = $destination_currency_id;

        return $this;
    }

    public function getSyncAt(): ?\DateTimeInterface
    {
        return $this->sync_at;
    }

    public function setSyncAt(\DateTimeInterface $sync_at): self
    {
        $this->sync_at = $sync_at;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
