<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 * @ORM\Table(name="currencies",indexes={@Index(name="base_idx", columns={"is_base"})})
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique="true")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_base;

    /**
     * @ORM\OneToMany(targetEntity=CurrencyRate::class, mappedBy="destination_currency_id")
     */
    private $created_at;

    public function __construct()
    {
        $this->created_at = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsBase(): ?bool
    {
        return $this->is_base;
    }

    public function setIsBase(bool $is_base): self
    {
        $this->is_base = $is_base;

        return $this;
    }

    /**
     * @return Collection|CurrencyRate[]
     */
    public function getCreatedAt(): Collection
    {
        return $this->created_at;
    }

    public function addCreatedAt(CurrencyRate $createdAt): self
    {
        if (!$this->created_at->contains($createdAt)) {
            $this->created_at[] = $createdAt;
            $createdAt->setDestinationCurrencyId($this);
        }

        return $this;
    }

    public function removeCreatedAt(CurrencyRate $createdAt): self
    {
        if ($this->created_at->removeElement($createdAt)) {
            // set the owning side to null (unless already changed)
            if ($createdAt->getDestinationCurrencyId() === $this) {
                $createdAt->setDestinationCurrencyId(null);
            }
        }

        return $this;
    }
}
