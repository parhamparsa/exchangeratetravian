<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\CurrencyRate;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CurrencyRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrencyRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrencyRate[]    findAll()
 * @method CurrencyRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CurrencyRate::class);
    }

    public function findByBaseAndCurrentCurrency(Currency $base, Currency $current): ?CurrencyRate
    {
        return $this->findOneBy(['base_currency_id' => $base->getId(), 'destination_currency_id' => $current->getId()]);
    }

    public function findByCurrencyAndDatetime(Currency $currency, string $dateTime): ?CurrencyRate
    {
        return $this->findOneBy(['destination_currency_id' => $currency->getId(), 'sync_at' => Carbon::parse($dateTime)]);
    }
}
