<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211028201123 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE currencies (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, is_base TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_37C446935E237E06 (name), INDEX base_idx (is_base), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency_rates (id INT AUTO_INCREMENT NOT NULL, base_currency_id INT NOT NULL, destination_currency_id INT NOT NULL, sync_at DATETIME NOT NULL, INDEX IDX_1336A95A3101778E (base_currency_id), INDEX IDX_1336A95A1D166A4D (destination_currency_id), INDEX currency_rate_idx (base_currency_id, destination_currency_id, sync_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE currency_rates ADD CONSTRAINT FK_1336A95A3101778E FOREIGN KEY (base_currency_id) REFERENCES currencies (id)');
        $this->addSql('ALTER TABLE currency_rates ADD CONSTRAINT FK_1336A95A1D166A4D FOREIGN KEY (destination_currency_id) REFERENCES currencies (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE currency_rates DROP FOREIGN KEY FK_1336A95A3101778E');
        $this->addSql('ALTER TABLE currency_rates DROP FOREIGN KEY FK_1336A95A1D166A4D');
        $this->addSql('DROP TABLE currencies');
        $this->addSql('DROP TABLE currency_rates');
    }
}
