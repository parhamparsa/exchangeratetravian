<?php

namespace App\Tests;

use App\Entity\Currency;
use PHPUnit\Framework\TestCase;

class CurrencyTest extends TestCase
{
    public function testCurrency(): void
    {
        $currency = new Currency();
        $name = "RUB";
        $isBase = false;

        $currency
            ->setName($name)
            ->setIsBase($isBase);

        $this->assertEquals($name, $currency->getName());
        $this->assertEquals($isBase, $currency->getIsBase());
    }
}
